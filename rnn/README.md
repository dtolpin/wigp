# Time Series Forcasting with RNN and ARIMA

For running ARIMA forcasting just run:
* `python ARIMA.py` it will run forcasting with all datasets and with windows size (start x size) of 10% of dataset size
* `python ARIMA.py -h` it will show you all available option 

For running RNN forcasting just run:
* `python rnn_runner.py` it will run forcasting with all datasets and with windows size (start x size) of 10% of dataset size
* `python rnn_runner.py -h` it will show you all available option 