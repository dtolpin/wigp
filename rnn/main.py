#!/usr/bin/env python3

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
from torch.autograd import Variable
from sklearn.preprocessing import StandardScaler
from RNN import RNN
import utils

NUM_EPOCHS = 2000
LR = 0.01
INPUT_START_INDEX = 1
INPUT_SIZE = 1
HIDDEN_SIZE = 3
NUM_LAYERS = 1
SLIDING_WINDOW_SIZE = 5

NUM_CLASSES = 1

PLOT=True
NLPD=True
LOG=False


def load_dataset_as_tensor(file, sc, training_percent=0.6, root_path="../data/"):
    time_series = pd.read_csv(root_path + file)

    time_series = time_series.iloc[:, INPUT_START_INDEX:INPUT_START_INDEX + INPUT_SIZE].values
    time_series = sc.fit_transform(time_series)
    x, y = utils.sliding_windows(time_series, SLIDING_WINDOW_SIZE)

    train_size = int(len(y) * training_percent)

    train_x = Variable(torch.Tensor(np.array(x[0:train_size])))
    train_y = Variable(torch.Tensor(np.array(y[0:train_size])))

    if train_size == len(y):
        return train_x, train_y, train_x, train_y

    test_x = Variable(torch.Tensor(np.array(x[train_size:len(x)])))
    test_y = Variable(torch.Tensor(np.array(y[train_size:len(y)])))
    return train_x, train_y, test_x, test_y


def train(x, y):
    rnn = RNN(NUM_CLASSES, INPUT_SIZE, HIDDEN_SIZE, NUM_LAYERS)

    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(rnn.parameters(), lr=LR)
    for epoch in range(NUM_EPOCHS):
        outputs = rnn(x)
        optimizer.zero_grad()
        loss = criterion(outputs, y)
        loss.backward()

        optimizer.step()
        if LOG and epoch % 100 == 0:
            print("Epoch: %d, loss: %1.5f" % (epoch, loss.item()))

    rnn.eval()
    return rnn


def test(x_test, y_test, model, plot=PLOT, nlpd=NLPD, name=None):
    test_predict = model(x_test)
    data_predict = test_predict.data.numpy()
    data_y = y_test.data.numpy()
    if plot:
        utils.plot(data_y, data_predict, sc, name)

    if nlpd:
        nlpd = np.sum(utils.nlpd(data_y, np.mean(data_predict), np.std(data_predict))) / len(data_predict)
        return nlpd


sc = StandardScaler()
results = []
for i in range(10):
    file_name = "trend-" + str(i) + ".csv"
    trainX, trainY, testX, testY = load_dataset_as_tensor(file_name, sc)
    model = train(trainX, trainY)
    nlpd = test(testX, testY, model, name="images/" + file_name)
    print(file_name, nlpd)
    results.append([file_name, nlpd])

for i in range(10):
    file_name = "trend+seasonal-" + str(i) + ".csv"
    trainX, trainY, testX, testY = load_dataset_as_tensor(file_name, sc)
    model = train(trainX, trainY)
    nlpd = test(testX, testY, model, name="images/" + file_name)
    print(file_name, nlpd)
    results.append([file_name, nlpd])


np.savetxt("nlpds.txt", np.array(results), fmt="%s")

