#!/usr/bin/env python3

import numpy as np
import pandas as pd
import sys, getopt
from statsmodels.tsa.arima.model import ARIMA
from sklearn.metrics import mean_squared_error
import utils

LOG = False
INPUT_START_INDEX = 1
INPUT_SIZE = 1
TRAINING_SET_SIZE = 0.6


def load_dataset(file, training_percent=TRAINING_SET_SIZE, root_path="../data/"):
    training_set = pd.read_csv(root_path + file)
    x = training_set.iloc[:, INPUT_START_INDEX:INPUT_START_INDEX+INPUT_SIZE].values[:, 0]
    x = (x - np.mean(x))/np.std(x)
    train_size = int(len(x) * training_percent)
    return x[0:train_size], x[train_size:len(x)]


def train(train, test):
    history = [x for x in train]
    predictions = list()
    nlpds = list()
    for t in range(len(test)):
        model = ARIMA(history, order=(1, 0, 1))
        model_fit = model.fit()
        output = model_fit.get_forecast()
        yhat = output.predicted_mean[0]
        std = output.se_mean[0]
        predictions.append(yhat)
        obs = test[t]
        history.append(obs)
        nlpds.append(utils.nlpd(obs, yhat, std))
        if LOG:
            print('predicted=%f, expected=%f, std=%f' % (yhat, obs, std))
    return predictions, nlpds


def trend(train_size):
    results = []
    # 10 Time series of Trend
    for i in range(10):
        file_name = "trend-" + str(i) + ".csv"
        x, y = load_dataset(file_name, training_percent=train_size)
        prediction_mean, nlpds = train(x, y)
        rmse = np.sqrt(mean_squared_error(y, prediction_mean))
        print('Test RMSE: %.3f' % rmse)
        nlpd = np.sum(nlpds) / len(y)
        print(file_name, nlpd)
        results.append([file_name, nlpd])
        skip_size = int((len(x) + len(y)) * train_size)
        utils.plot(y[skip_size:], prediction_mean[skip_size:], name="images/arima_" + file_name)

    data = np.array(results)
    mean = np.mean(np.array(data[:,1], dtype=float))
    std = np.std(np.array(data[:,1], dtype=float))
    results.append([str(mean), "+-" + str(std)])
    np.savetxt("arima_trend_nlpds.txt", np.array(results), fmt="%s")


def trend_seasonal(train_size=0.6):
    # 10 Time series of Trend+Seasonal
    results = []
    for i in range(10):
        file_name = "trend+seasonal-" + str(i) + ".csv"
        x, y = load_dataset(file_name, training_percent=train_size)
        prediction_mean, nlpds = train(x, y)
        rmse = np.sqrt(mean_squared_error(y, prediction_mean))
        print('Test RMSE: %.3f' % rmse)
        nlpd = np.sum(nlpds) / len(y)
        print(file_name, nlpd)
        results.append([file_name, nlpd])
        skip_size = int((len(x) + len(y)) * train_size)
        utils.plot(y[skip_size:], prediction_mean[skip_size:], name="images/arima_" + file_name)

    data = np.array(results)
    mean = np.mean(np.array(data[:,1], dtype=float))
    std = np.std(np.array(data[:,1], dtype=float))
    results.append([str(mean), "+-" + str(std)])
    np.savetxt("arima_trend+seasonal_nlpds.txt", np.array(results), fmt="%s")


def marathon(train_size=0.4):
    # Marathon dataset
    results = []
    file_name = "marathon-men-gold.csv"
    x, y = load_dataset(file_name, training_percent=train_size)
    prediction_mean, nlpds = train(x, y)
    rmse = np.sqrt(mean_squared_error(y, prediction_mean))
    print('Test RMSE: %.3f' % rmse)
    nlpd = np.sum(nlpds) / len(y)
    print(file_name, nlpd)
    results.append([file_name, nlpd])
    skip_size = int((len(x) + len(y)) * train_size)
    utils.plot(y[skip_size:], prediction_mean[skip_size:], name="images/arima_" + file_name)
    np.savetxt("arima_marathon-men-gold_nlpd.txt", np.array(results), fmt="%s")


def motocycle(train_size=0.1):
    # Motocycle dataset
    results = []
    file_name = "motor.csv"
    x, y = load_dataset(file_name, training_percent=train_size)
    prediction_mean, nlpds = train(x, y)
    rmse = np.sqrt(mean_squared_error(y, prediction_mean))
    print('Test RMSE: %.3f' % rmse)
    nlpd = np.sum(nlpds) / len(y)
    print(file_name, nlpd)
    results.append([file_name, nlpd])
    skip_size = int((len(x) + len(y)) * train_size)
    utils.plot(y[skip_size:], prediction_mean[skip_size:], name="images/arima_" + file_name)
    np.savetxt("arima_motor_nlpd.txt", np.array(results), fmt="%s")


def lidar(train_size=0.1):
    # LIDAR dataset
    results = []
    file_name = "lidar.csv"
    x, y = load_dataset(file_name, training_percent=train_size)
    prediction_mean, nlpds = train(x, y)
    rmse = np.sqrt(mean_squared_error(y, prediction_mean))
    print('Test RMSE: %.3f' % rmse)
    nlpd = np.sum(nlpds) / len(y)
    print(file_name, nlpd)
    results.append([file_name, nlpd])
    skip_size = int((len(x) + len(y)) * train_size)
    utils.plot(y[skip_size:], prediction_mean[skip_size:], name="images/arima_" + file_name)
    np.savetxt("arima_lidar_nlpd.txt", np.array(results), fmt="%s")


def main(argv):
    ex_type = 'all'
    size = 0.2
    try:
        opts, args = getopt.getopt(argv, "ht:s:", ["type=", "window_size="])
    except getopt.GetoptError:
        print('ARIMA.py <type> <window_size: 0.1, 0.2 ... 0.9>')
        print('Default window_size is 0.2')
        sys.exit(2)
    for opt, arg in opts:
        print(arg)
        if opt == '-h':
            print('ARIMA.py -t all|t|ts|motor|mar|lidar -s 0.1')
            print('all - run all experiments')
            print('t - run trend experiments')
            print('ts - run treand+seasonal experiments')
            print('motor - run motorcycle experiments')
            print('mar - run marathon experiments')
            print('lidar - run LIDAR experiments')
            sys.exit()
        elif opt in ("-t"):
            ex_type = arg
        elif opt in ("-s"):
            size = float(arg)

    if ex_type == 'all':
        trend(size)
        trend_seasonal(size)
        motocycle(size)
        marathon(size)
        lidar(size)
    elif ex_type == 't':
        trend(size)
    elif ex_type == 'ts':
        trend_seasonal(size)
    elif ex_type == 'motor':
        motocycle(size)
    elif ex_type == 'mar':
        marathon(size)
    elif ex_type == 'lidar':
        lidar(size)


if __name__ == "__main__":
   main(sys.argv[1:])

