#!/usr/bin/env python3

import numpy as np
import pandas as pd
import torch
import sys, getopt
import torch.nn as nn
from torch.autograd import Variable
from sklearn.preprocessing import StandardScaler
from RNN import RNN
import utils

NUM_EPOCHS = 1000
LR = 0.01
INPUT_START_INDEX = 1
INPUT_SIZE = 1
HIDDEN_SIZE = 3
NUM_LAYERS = 1
SLIDING_WINDOW_SIZE = 20

NUM_CLASSES = 1

PLOT=False
NLPD=False
LOG=False


def load_dataset(file, sc, root_path="../data/"):
    time_series = pd.read_csv(root_path + file)

    time_series = time_series.iloc[:, INPUT_START_INDEX:INPUT_START_INDEX + INPUT_SIZE].values
    time_series = sc.fit_transform(time_series)

    return time_series


def split_train_test(time_series, size=SLIDING_WINDOW_SIZE):
    train_x = time_series[:size + 1]
    x, y = utils.sliding_windows(train_x, 1)
    train_x = Variable(torch.Tensor(np.array(x)))
    train_y = Variable(torch.Tensor(np.array(y)))
    return train_x, train_y


def train(x, y, nlpd=NLPD):
    rnn = RNN(NUM_CLASSES, INPUT_SIZE, HIDDEN_SIZE, NUM_LAYERS)

    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(rnn.parameters(), lr=LR)
    for epoch in range(NUM_EPOCHS):
        outputs = rnn(x)
        optimizer.zero_grad()
        loss = criterion(outputs, y)
        loss.backward()

        optimizer.step()
        if LOG and epoch % 100 == 0:
            print("Epoch: %d, loss: %1.5f" % (epoch, loss.item()))
    rnn.eval()
    with torch.no_grad():
        test_predict = rnn(x)
        data_predict = test_predict.data.numpy()
        data_y = y.data.numpy()
        if nlpd:
            nlpd = np.sum(utils.nlpd(data_y[-1], np.mean(data_predict), np.std(data_predict)))
            return rnn, data_y[-1], data_predict[-1], nlpd
        return rnn, data_y[-1], np.mean(data_predict), np.std(x.data.numpy())


def run_forcasting(file_name, scaller, size=SLIDING_WINDOW_SIZE):
    ts = load_dataset(file_name, scaller)
    nlpds = 0
    original = []
    for i in range(len(ts) - size):
        tx, ty = split_train_test(ts, size=i + size)
        model, y, m, s = train(tx, ty)
        original.append(y)
        nlpds = nlpds + utils.nlpd(y, m, s)
        print("Interation: " + str(i + 1) + " of " + str(len(ts) - size))
        if (i + 1) == len(ts) - size:
            nlpds = np.sum(nlpds)/len(original)
            predict = model(tx)
            utils.plot(ty[size:], predict.data.numpy()[size:], scaler=scaller, name="images/rnn_" + file_name)
    print(file_name + " ", nlpds)
    return [file_name, nlpds]


def trend(size):
    # 10 Time series of Trend
    scaller = StandardScaler()
    results = []
    for j in range(10):
        file_name = "trend-" + str(j) + ".csv"
        r = run_forcasting(file_name, scaller, size=size)
        results.append(r)

    data = np.array(results)
    mean = np.mean(np.array(data[:,1], dtype=float))
    std = np.std(np.array(data[:,1], dtype=float))
    results.append([str(mean), "+-" + str(std)])
    np.savetxt("rnn_trend_nlpds.txt", np.array(results), fmt="%s")


def trend_seasonal(size):
    # 10 Time series of Trend+Seasonal
    scaller = StandardScaler()
    results = []
    for j in range(10):
        file_name = "trend+seasonal-" + str(j) + ".csv"
        r = run_forcasting(file_name, scaller, size=size)
        results.append(r)

    data = np.array(results)
    mean = np.mean(np.array(data[:,1], dtype=float))
    std = np.std(np.array(data[:,1], dtype=float))
    results.append([str(mean), "+-" + str(std)])
    np.savetxt("rnn_trend+seasonal_nlpds.txt", np.array(results), fmt="%s")


def marathon(size):
    # Marathon dataset
    scaller = StandardScaler()
    results = []
    file_name = "marathon-men-gold.csv"
    r = run_forcasting(file_name, scaller, size=size)
    results.append(r)
    np.savetxt("rnn_marathon-men-gold_nlpds.txt", np.array(results), fmt="%s")


def motocycle(size):
    # Motocycle dataset
    scaller = StandardScaler()
    results = []
    file_name = "motor.csv"
    r = run_forcasting(file_name, scaller, size=size)
    results.append(r)
    np.savetxt("rnn_motor_nlpds.txt", np.array(results), fmt="%s")


def lidar(size):
    # LIDAR dataset
    scaller = StandardScaler()
    results = []
    file_name = "lidar.csv"
    r = run_forcasting(file_name, scaller, size=size)
    results.append(r)
    np.savetxt("rnn_lidar_nlpds.txt", np.array(results), fmt="%s")


def main(argv):
    ex_type = 'all'
    size = 20
    try:
        opts, args = getopt.getopt(argv, "ht:s:", ["type=", "window_size="])
    except getopt.GetoptError:
        print('rnn_runner.py <type> <window_size: 10 etc.>')
        print('Default window_size is 0.2')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('rnn_runner.py -t all|t|ts|motor|mar|lidar -s 10')
            print('all - run all experiments')
            print('t - run trend experiments')
            print('ts - run treand+seasonal experiments')
            print('motor - run motorcycle experiments')
            print('mar - run marathon experiments')
            print('lidar - run LIDAR experiments')
            sys.exit()
        elif opt in ("-t"):
            ex_type = arg
        elif opt in ("-s"):
            size = int(arg)

    if ex_type == 'all':
        trend(size)
        trend_seasonal(size)
        motocycle(size)
        marathon(size)
        lidar(size)
    elif ex_type == 't':
        trend(size)
    elif ex_type == 'ts':
        trend_seasonal(size)
    elif ex_type == 'motor':
        motocycle(size)
    elif ex_type == 'mar':
        marathon(size)
    elif ex_type == 'lidar':
        lidar(size)


if __name__ == "__main__":
   main(sys.argv[1:])
