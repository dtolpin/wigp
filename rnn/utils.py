#!/usr/bin/env python3

import torch
import numpy as np
import matplotlib.pyplot as plt


def sliding_windows(data, seq_length):
    x = []
    y = []

    for i in range(len(data)-seq_length):
        _x = data[i:(i+seq_length)]
        _y = data[i+seq_length]
        x.append(_x)
        y.append(_y)

    return np.array(x), np.array(y)


def get_confidence_intervals(predictions, interval, scaler):

    predictions = torch.tensor(predictions)

    prediction_mean = predictions.mean(1)
    prediction_std = predictions.std(-2).detach().cpu().numpy()

    prediction_std = torch.tensor(prediction_std)

    upper_bound = prediction_mean - (prediction_std * interval)
    lower_bound = prediction_mean + (prediction_std * interval)

    pred_mean_final = prediction_mean.unsqueeze(1).detach().cpu().numpy()
    pred_mean_unscaled = scaler.inverse_transform(pred_mean_final)

    upper_bound_unscaled = upper_bound.unsqueeze(1).detach().cpu().numpy()
    upper_bound_unscaled = scaler.inverse_transform(upper_bound_unscaled)

    lower_bound_unscaled = lower_bound.unsqueeze(1).detach().cpu().numpy()
    lower_bound_unscaled = scaler.inverse_transform(lower_bound_unscaled)

    return pred_mean_unscaled, upper_bound_unscaled, lower_bound_unscaled


def plot(original, prediction, scaler=None, name=None):
    if scaler is not None:
        pred_mean_unscaled, upper_bound_unscaled, lower_bound_unscaled = get_confidence_intervals(prediction, 0.68, scaler)
        upper_bound_unscaled = upper_bound_unscaled[:, 0]
        lower_bound_unscaled = lower_bound_unscaled[:, 0]
        _, upper_bound_unscaled95, lower_bound_unscaled95 = get_confidence_intervals(prediction, 0.95, scaler)
        upper_bound_unscaled95 = upper_bound_unscaled95[:, 0]
        lower_bound_unscaled95 = lower_bound_unscaled95[:, 0]
        data_y_plot = scaler.inverse_transform(original)
    else:
        pred_mean_unscaled = prediction
        prediction_std = np.std(prediction)
        upper_bound_unscaled = prediction - (prediction_std * 0.68)
        lower_bound_unscaled = prediction + (prediction_std * 0.68)
        upper_bound_unscaled95 = prediction - (prediction_std * 0.95)
        lower_bound_unscaled95 = prediction + (prediction_std * 0.95)
        data_y_plot = original

    plt.scatter(np.arange(len(data_y_plot)), data_y_plot, marker='o', color='black', label="Original")

    plt.plot(pred_mean_unscaled, c='black', label="Predicted")

    plt.fill_between(x=np.arange(len(upper_bound_unscaled)),
                     y1=upper_bound_unscaled,
                     y2=lower_bound_unscaled,
                     facecolor='gray',
                     label="68%",
                     alpha=0.2)

    plt.fill_between(x=np.arange(len(upper_bound_unscaled95)),
                     y1=upper_bound_unscaled95,
                     y2=lower_bound_unscaled95,
                     facecolor='gray',
                     label="95%",
                     alpha=0.1)

    plt.legend()
    if name is not None:
        plt.savefig(name + '.png')
    plt.show()


def nlpd(y, mean, std):
    vari = std**2
    logv = np.log(vari)
    d = y - mean
    return 0.5 * (np.log(2) * np.log(np.pi) + (d**2/vari) + logv)


def tensor_nlpd(y):
    predictions = torch.tensor(y)

    mean = predictions.mean(1).detach().cpu().numpy()
    std = predictions.std(-2).detach().cpu().numpy()
    vari = std**2
    logv = np.log(vari)
    d = predictions - mean

    return 0.5 * (np.log(2) * np.log(np.pi) + (d**2/vari) + logv)
