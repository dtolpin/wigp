#!/usr/bin/env python

import csv
import sys
import re
import numpy

rdr = csv.reader(sys.stdin)
data = {}
for record in rdr:
    name, alg, nldp = record
    name = name.split("-")[0]
    nldp = float(nldp)
    if name not in data:
        data[name] = {}
    if alg not in data[name]:
        data[name][alg] = []
    data[name][alg].append(nldp)
for name in data.keys():
    for alg in data[name].keys():
        mean = numpy.mean(data[name][alg])
        std = numpy.std(data[name][alg])
        print("{},{},{:.6f},{:.6f}".format(name, alg, mean, std))
