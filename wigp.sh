#!/bin/sh

NITER=${NITER:-1000}

for datasets in motor lidar marathon-men-gold trend-[0-9] trend+seasonal-[0-9]; do
	for dataset in data/$datasets.csv; do
		echo $dataset
		./wigp -n $NITER < $dataset | sed -l 's/,/ /g' > `echo $dataset|sed 's/csv/pred/'`
	done &
done
wait
for datasets in motor lidar marathon-men-gold trend-[0-9] trend+seasonal-[0-9]; do
	for dataset in data/$datasets.csv; do
		echo $dataset (seasonal)
		./wigp -n $NITER -seasonal < $dataset | sed -l 's/,/ /g' > `echo $dataset|sed 's/csv/seasonal.pred/'`
	done &
done
wait
for datasets in motor lidar marathon-men-gold trend-[0-9] trend+seasonal-[0-9]; do
	for dataset in data/$datasets.csv; do
		echo $dataset (no warping)
		./wigp -n $NITER -no-warping < $dataset | sed -l 's/,/ /g' > `echo $dataset|sed 's/csv/no-warp.pred/'`
	done &
done
wait
