module bitbucket.org/dtolpin/wigp

go 1.14

require (
	bitbucket.org/dtolpin/gogp v0.3.3
	bitbucket.org/dtolpin/infergo v0.9.3
	gonum.org/v1/gonum v0.8.2
)
